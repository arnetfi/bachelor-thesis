library(dplyr)
library(ggplot2)
library(lubridate)
library(tidyverse)

# read in data
data <- readRDS("training_events.rds")
data <- data %>% drop_na(DELAY)

S2_Stations <- c('Z�rich Flughafen', 'Z�rich Oerlikon', 
                 'Z�rich HB', 'Z�rich Wiedikon', 'Z�rich Enge', 
                 'Thalwil', 'Horgen', 'W�denswil', 'Richterswil', 
                 'Pf�ffikon SZ', 'Altendorf', 'Lachen', 
                 'Siebnen-Wangen', 'Sch�belbach-Buttikon', 'Reichenburg', 
                 'Bilten','Ziegelbr�cke', 'Unterterzen')

iteration <- length(S2_Stations)-1

# extract all the train names from data 
data <- as_tibble(data)

line <- data %>%
  filter(LINE_TEXT == 'S2') %>%
  filter(STATION %in% S2_Stations)

start_stations <- line %>%
  pull(START_STOP) %>%
  unique()

dir_1 <- c()
dir_2 <- c()

# filter directionwise
for (start in start_stations) {
    cur_line <- line %>%
      filter(START_STOP == start) %>%
      filter(EVENT_TYPE == 'ARRIVAL')
    
    run_id <- cur_line %>%
      select(RUN_ID) %>%
      pull() %>%
      unique()
    
    if (length(run_id) > 1) {
      random_id <- sample(run_id, 1)
      
      cur_line <- cur_line %>%
        filter(RUN_ID == random_id)
      
      cur_stations <- cur_line %>%
        select(STATION) %>%
        pull()
      
      cur_station_nrs <- cur_line %>%
        select(STATION_NR) %>%
        unique() %>%
        pull()
      
      if (length(cur_station_nrs) >= 2) {
        sample_station_nrs <- sort(sample(cur_station_nrs, 2), decreasing = FALSE)
        first_station <- cur_line %>%
          filter(STATION_NR == sample_station_nrs[1]) %>%
          select(STATION) %>%
          pull()
        
        second_station <- cur_line %>%
          filter(STATION_NR == sample_station_nrs[2]) %>%
          select(STATION) %>%
          pull()
        
        first_station_index <- which(S2_Stations == first_station)
        second_station_index <- which(S2_Stations == second_station)
        
        if (first_station_index < second_station_index) {
          dir_1 <- c(dir_1, start)
        }
        else {
          dir_2 <- c(dir_2, start)
        }
    }
  }
}

print(dir_1)
print(dir_2)

line_down <- line %>%
  filter(START_STOP %in% dir_1) 

line_up <- line %>%
  filter(START_STOP %in% dir_2)


for (i in 1:iteration) {
  station_a <- S2_Stations[i]
  station_b <- S2_Stations[i+1]
  
  if (station_a == 'Z�rich Flughafen') {
    sector <- line_down %>%
      filter(STATION == station_a & EVENT_TYPE == 'DEPARTURE'
             | STATION == station_b & EVENT_TYPE == 'ARRIVAL')
  }
  else {
    sector <- line_down %>%
      filter(STATION == station_a & EVENT_TYPE == 'ARRIVAL' 
             | STATION == station_b & EVENT_TYPE=='ARRIVAL')
  }
  
  valid_sector <- sector %>%
    group_by(RUN_ID) %>%
    summarise(counter = n()) %>%
    ungroup() %>%
    filter(counter == 2) %>%
    select(RUN_ID) %>%
    pull()
  
  sector <- sector %>%
    filter(RUN_ID %in% valid_sector)
  
  if (i < 10) {
    file_name_rds <- paste('S2/Dir_1_1/', 0, i, 'S2Down', station_a, station_b, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  else {
    file_name_rds <- paste('S2/Dir_1_1/', i, 'S2Down', station_a, station_b, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  
  saveRDS(sector, file_name_rds)
}

for (i in 1:iteration) {
  station_a <- S2_Stations[i]
  station_b <- S2_Stations[i+1]
  
  if (station_b == 'Unterterzen') {
    sector <- line_up %>%
      filter(STATION == station_a & EVENT_TYPE == 'ARRIVAL'
             | STATION == station_b & EVENT_TYPE == 'DEPARTURE')
  }
  else {
    sector <- line_up %>%
      filter(STATION == station_a & EVENT_TYPE == 'ARRIVAL' 
             | STATION == station_b & EVENT_TYPE=='ARRIVAL')
  }
  
  valid_sector <- sector %>%
    group_by(RUN_ID) %>%
    summarise(counter = n()) %>%
    ungroup() %>%
    filter(counter == 2) %>%
    select(RUN_ID) %>%
    pull()
  
  sector <- sector %>%
    filter(RUN_ID %in% valid_sector)
  
  if (i < 10) {
    file_name_rds <- paste('S2/Dir_2_1/', 0, i, 'S2Up', station_b, station_a, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  else {
    file_name_rds <- paste('S2/Dir_2_1/', i, 'S2Up', station_b, station_a, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  
  saveRDS(sector, file_name_rds)
}

