library(dplyr)
library(ggplot2)
library(lubridate)
library(tidyverse)

# read in data
data <- readRDS("training_events.rds")
data <- data %>% drop_na(DELAY)

S24_Stations <- c('Thayngen', 'Herblingen', 'Schaffhausen', 'Neuhausen',
                  'Andelfingen', 'Winterthur', 'Weinfelden', 'M�rstetten',
                  'M�llheim-Wigoltingen', 'H�ttlingen-Mettendorf', 
                  'Felben-Wellhausen', 'Frauenfeld', 'Islikon', 
                  'Rickenbach-Attikon', 'Wiesendangen', 'Oberwinterthur',
                  'Winterthur', 'Kemptthal', 'Effretikon', 'Bassersdorf',
                  'Z�rich Flughafen', 'Z�rich Oerlikon', 'Z�rich Wipkingen',
                  'Z�rich HB', 'Z�rich Wiedikon', 'Z�rich Enge',
                  'Z�rich Wollishofen', 'Kilchberg', 'R�schlikon',
                  'Thalwil', 'Oberrieden Dorf', 'Horgen Oberdorf',
                  'Baar', 'Zug')

iteration <- length(S24_Stations)-1

# extract all the train names from data 
data <- as_tibble(data)

line <- data %>%
  filter(LINE_TEXT == 'S24') %>%
  filter(STATION %in% S24_Stations)

enge_down <- line %>%
 filter(DIR == 'DOWN') %>%
 filter(STATION == 'Z�rich Enge')

enge_up <- line %>%
 filter(DIR == 'UP') %>%
 filter(STATION == 'Z�rich Enge')

saveRDS(enge_down, 'S24/enge_down.rds')
saveRDS(enge_up, 'S24/enge_up.rds')

start_stations <- line %>%
  pull(START_STOP) %>%
  unique()

dir_1 <- c()
dir_2 <- c()

for (start in start_stations) {
  # only look at runs with start station == start
  cur_line <- line %>%
    filter(START_STOP == start) %>%
    filter(EVENT_TYPE == 'ARRIVAL')
  
  # select all RUN_ID that have this start station
  run_id <- cur_line %>%
    select(RUN_ID) %>%
    pull() %>%
    unique()
  
  if (length(run_id) > 1) {
    # sample one random run
    random_id <- sample(run_id, 1)
    
    cur_line <- cur_line %>%
      filter(RUN_ID == random_id)
    
    # select all stations from this run
    cur_stations <- cur_line %>%
      select(STATION) %>%
      pull()
    
    # select all station numbers from this RUN_ID
    cur_station_nrs <- cur_line %>%
      select(STATION_NR) %>%
      unique() %>%
      pull()
    
    if (length(cur_station_nrs) >= 2) {
      sample_station_nrs <- sort(sample(cur_station_nrs, 2), decreasing = FALSE)
      first_station <- cur_line %>%
        filter(STATION_NR == sample_station_nrs[1]) %>%
        select(STATION) %>%
        pull()
      
      second_station <- cur_line %>%
        filter(STATION_NR == sample_station_nrs[2]) %>%
        select(STATION) %>%
        pull()
      
      first_station_index <- which(S24_Stations == first_station)
      second_station_index <- which(S24_Stations == second_station)
      
      if (first_station_index < second_station_index) {
        dir_1 <- c(dir_1, start)
      }
      else {
        dir_2 <- c(dir_2, start)
      }
    }
  }
}

print(dir_1)
print(dir_2)

line_1 <- line %>%
  filter(START_STOP %in% dir_1) 

line_2 <- line %>%
  filter(START_STOP %in% dir_2)


for (i in 1:iteration) {
  station_a <- S24_Stations[i]
  station_b <- S24_Stations[i+1]
  
  if (station_a == 'Thayngen') {
    sector <- line_1 %>%
      filter(STATION == station_a & EVENT_TYPE == 'DEPARTURE'
             | STATION == station_b & EVENT_TYPE == 'ARRIVAL')
  }
  else {
    sector <- line_1 %>%
      filter(STATION == station_a & EVENT_TYPE == 'ARRIVAL' 
             | STATION == station_b & EVENT_TYPE=='ARRIVAL')
  }
  
  valid_sector <- sector %>%
    group_by(RUN_ID) %>%
    summarise(counter = n()) %>%
    ungroup() %>%
    filter(counter == 2) %>%
    select(RUN_ID) %>%
    pull()
  
  sector <- sector %>%
    filter(RUN_ID %in% valid_sector)
  
  if (i < 10) {
    file_name_rds <- paste('S24/Dir_1_1/', 0, i, 'S24Down', station_a, station_b, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  else {
    file_name_rds <- paste('S24/Dir_1_1/', i, 'S24Down', station_a, station_b, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  
  saveRDS(sector, file_name_rds)
}

for (i in 1:iteration) {
  station_a <- S24_Stations[i]
  station_b <- S24_Stations[i+1]
  
  if (station_b == 'Zug') {
    sector <- line_2 %>%
      filter(STATION == station_a & EVENT_TYPE == 'ARRIVAL'
             | STATION == station_b & EVENT_TYPE == 'DEPARTURE')
  }
  else {
    sector <- line_2 %>%
      filter(STATION == station_a & EVENT_TYPE == 'ARRIVAL' 
             | STATION == station_b & EVENT_TYPE=='ARRIVAL')
  }
  
  valid_sector <- sector %>%
    group_by(RUN_ID) %>%
    summarise(counter = n()) %>%
    ungroup() %>%
    filter(counter == 2) %>%
    select(RUN_ID) %>%
    pull()
  
  sector <- sector %>%
    filter(RUN_ID %in% valid_sector)
  
  if (i < 10) {
    file_name_rds <- paste('S24/Dir_2_1/', 0, i, 'S24Up', station_b, station_a, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  else {
    file_name_rds <- paste('S24/Dir_2_1/', i, 'S24Up', station_b, station_a, '.rds', sep='')
    file_name_rds <- str_replace_all(file_name_rds, " ", "")
  }
  
  saveRDS(sector, file_name_rds)
}

