# Time Series Analysis for Train Delay Prediction

## Files
There are three files each of which trains one of the three models.

##### Non-Interacting Model
_Parameter value that need to be chosen_
- Train service: S2, S8 or S24
- Direction: Down or Up (North-South or South-North direction)
- p: how many preceding stations should be included (1, 2 or 3)
- k: k-parameter for k-fold cross validation

_Input_
Depending on the parameter values that are chosen, the input is given as a set of files containing the necessary information to train the model for every station of the respective train and direction.

_Output_
Calculated:
- Parameters (a_1, ..., a_p and c_k) for each station k
- Delay predictions for test set 

Stored: several dictionaries containing information about MAEs and parameters values trained:
- MAE of non-interacting model at every station of respective train service and direction
- MAE of baseline approach at every station
- Trained parameters of non-interacting model for every station of respective train service and direction
- Standard deviations of trained parameters at every station of respective train service and direction

##### Intercating Model
_Parameter values that need to be chosen_
- k (int): k-parameter for k-fold cross validation
- headway (int): maximum allowed scheduled headway between train j and train i [min]
- threshold (int or float): minimal delay that train j needs to have for it to be considered to have a knock-on influence
- train_on_knockOn (boolean): if set true, the non-interacting model is trained on runs where a knock-on delay was detected only. If set false, it is traind on all data.
- predict_S24_delay (boolean): if set true, S24 delay in Enge is predicted, else the actual delay value is taken

_Input_
- Dataframe with arrival events of S24 and S8 service in Enge
- Trained parameters (a and c) of S24 on section Zürich Wollishofen - Zürich Enge
- Dataframe with information about S2 on section Thalwil-Zürich Enge
- Dataframe with information about S24 on section Thalwil-Zürich Enge

_Output_
Calculated: 
- Parameters a_1 and c_k for S2 service in South-North direction at station Enge for non-interacting model
- Parameters a_1, b and c_k for S2 service in South-North direction at station Enge for interacting model
- Delay predictions with both models for test set 

Stored: Two dataframes, one containing operating figures to analyse and assess the performance of the interacting model and one containing operating figures to analyse and assess the performance of the non-interacting model:
- MAE of the model
- MAE of the baseline approach
- Trained parameters
- Standard deviations of trained parameters

##### Binary Decision Model
_Parameters that need to be chosen_
- k (int): k-parameter for k-fold cross validation
- headway (int): maximum allowed scheduled headway between train j and train i [min]
- threshold (int or float): minimal delay that train j needs to have for it to be considered to have a knock-on influence
- predict_S24_delay (boolean): if set true, S24 delay in Enge is predicted, else the actual delay value is taken

_Input_
- Dataframe with arrival events of S24 and S8 service in Enge
- Trained parameters (a and c) of S24 on section Zürich Wollishofen - Zürich Enge
- Dataframe with information about S2 on section Thalwil-Zürich Enge
- Dataframe with information about S24 on section Thalwil-Zürich Enge

_Output_
Calculated:
- Parameters a, b and c for S2 service in South-North direction at station Enge
- Delay predictions for test set 

Stored: Dataframe containing operating figures to analyse the model and assess its performance:
- MAE of the model
- MAE of the baseline approach
- Trained parameters
- Standard deviations of trained parameters